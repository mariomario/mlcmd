      PROGRAM SINT  !! Sintesi per ammassi stellari v. 1.0      
      IMPLICIT REAL*8 (A-H,O-Z)
  231 FORMAT(//,' ****************************')
  232 FORMAT(' ****************************',//)
    1   WRITE(*,231)
        WRITE(*,*)'* (1) STOP                 *'
        WRITE(*,*)'* (2) SYNTHESYS WITH HB    *'
        WRITE(*,*)'* (3) SYNTHESYS WITHOUT HB *'
        WRITE(*,*)'* (4) TRACK INTERPOLATION  *'
        WRITE(*,232)
        READ(*,*)I
        IF(I.EQ.1) STOP
        IF(I.EQ.2.OR.I.EQ.3) THEN
           CALL READ2(TMIN,TMAX,I)
           CALL SINT2(TMIN,TMAX,VT,NPU,I,BIN,VL)
           CALL WRITE2(VT,NPU,BIN,VL)
        END IF
        IF(I.EQ.4) THEN
           CALL READ2(TMIN,TMAX,I)
           CALL TRAK
        END IF
        stop
      END
      SUBROUTINE CUB(ALFA,BETA,GAMMA,DELTA)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/PRIV/A1,A2,A3,A4,B1,B2,B3,B4
      D1=B2*B2+B2*B1
      C1=B3*B3+B3*B1-D1
      C2=B3-B2
      D2=(A2-A1)/(B2-B1)
      C3=(A3-A1)/(B3-B1)-D2
      C4=B4*B4+B4*B1-D1
      C5=B4-B2
      C6=(A4-A1)/(B4-B1)-D2
      D3=C1*C5-C2*C4
      ALFA=(C3*C5-C2*C6)/D3
      BETA=(C1*C6-C4*C3)/D3
      GAMMA=D2-ALFA*(D1+B1*B1)-BETA*(B2+B1)
      DELTA=A1-ALFA*B1*B1*B1-BETA*B1*B1-GAMMA*B1
      RETURN
      END
      SUBROUTINE READ2 (TMIN,TMAX,INDI)
c  ****************** MEANING OF VARIABLES *******************
c  *  NMAS   numero totale tracce utilizzate                 *
c  *  NLOW   numero tracce fino a X=0                        *
c  *  NHB    numero tracce di HB                             *
c  *  NSHORT numero punti per traccia fino a X=0             *
c  *  NPHB   numero punti per HB                             *
c  *  NLONG  numero totale punti per traccia                 *
c  *  NINTE  numero totale intervalli per traccia            *
c  *  NBAND  numero bande fotometriche                       *
c  ***********************************************************
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (IPUN=500000,IMS=20,INT=10,IHB=40)
      CHARACTER*31 NAM1(IMS),NAM2(IHB)
      CHARACTER*300 TESTO
      DIMENSION AMAG(IPUN,10,IMS),AHBMAG(IPUN,10,IHB)
      COMMON/COM1/DATA(IPUN,2),ETA(IPUN)
      COMMON/COM2/TRK(IPUN,2,IMS),VMAS(IMS),AGE(IPUN,IMS)
      COMMON/COM3/THB(IPUN,2,IHB),CORE(IHB),AHB(IPUN,IHB)
      COMMON/COM6/NMAS,NLOW,NSHORT,NLONG,NINTE,NHB,NPHB
      COMMON/COM7/VM(INT)
  200 FORMAT(F18.11,34X,7F8.4)
  202 FORMAT(I5,F18.11,34X,7F8.4)

  201 FORMAT(F18.11,23X,7F8.4)
  100 FORMAT(F12.8,27X,2F9.5)
  101 FORMAT(8I5)
  102 FORMAT(F8.4,1X,A31)
  104 FORMAT(1X,'MASSES:',F8.4,3X,'FILENAMES:',A31)
  106 FORMAT(I1)
  107 FORMAT('  SYNTESYS AVAILABLE IN THE RANGE ',F8.1,' - ',F8.1,'
     #  MYRS')
  108 FORMAT(10I5)
  109 FORMAT(1X,9A8)
      OPEN(7,FILE='initial.trk',STATUS='OLD')
      READ(7,101) NMAS, NLOW, NSHORT, NLONG, NINTE, NHB, NPHB, NBAND
c      write(*,101) NMAS, NLOW, NSHORT, NLONG, NINTE, NHB, NPHB, NBAND
c      pause
C=================================================================== 
C=== READING EVOLUTIONARY TRACK FOR CORE AND SHELL H-BURNING STAGE = 
C===================================================================    
       READ(7,'(A300)') TESTO
       DO J=1,NMAS
        READ(7,102) VMAS(J),NAM1(J) 
        WRITE(*,104) VMAS(J),NAM1(J)
       END DO
C=================================================================== 
C== READING EVOLUTIONARY TRACK FOR CORE AND SHELL HE-BURNING STAGE = 
C===================================================================     
       IF(INDI.EQ.2) THEN
       READ(7,'(A300)') TESTO
        DO J=1,NHB
         READ(7,102) CORE(J),NAM2(J) 
         WRITE(*,104) CORE(J),NAM2(J) 
c         pause
        END DO
      END IF
C===================================================================            
      CLOSE (7)
      DO IM = 1 , NMAS
          IF(IM.LE.NLOW) THEN
              MAX=NSHORT
          ELSE
              MAX=NLONG
          ENDIF
          WRITE(*,*)' READING NOW ... ',NAM1(IM)
          OPEN(3,FILE=NAM1(IM),STATUS='OLD')
          REWIND 3
          IF(INDI.LE.4) THEN
            DO J=1,8
             READ(3,'(A300)') TESTO
            END DO 
            DO I=1,MAX
              READ(3,200) AGE(I,IM),(AMAG(I,J,IM),J=1,NBAND)
c              write(*,202) I,AGE(I,IM),(AMAG(I,J,IM),J=1,NBAND)
C===================================================================            
C   1) F435W 2) F475W 3) F555W 4) F606W 5) F625W 6) F775W 7) F814W         
C===================================================================            
              trk(i,1,im)=AMAG(I,7,IM) ! F814W
              trk(I,2,IM)=AMAG(I,4,IM)-trk(I,1,IM) ! (F606W-F814W)
            END DO
c            pause
          ELSE
              WRITE(*,*)'ERROR: WRONG CHOICE '
              STOP
          END IF
          CLOSE(3)
      END DO
      IF(INDI.EQ.2) THEN
        DO IH = 1 , NHB
          WRITE(*,*)' READING NOW ... ',NAM2(IH)
          OPEN(3,FILE=NAM2(IH),STATUS='OLD')
          REWIND 3
          
            DO J=1,8
             READ(3,'(A300)') TESTO
            END DO 
         DO I=1,NPHB
          READ(3,201) AHB(I,IH), (AHBMAG(I,J,IH),J=1,NBAND)
c          WRITE(*,201) AHB(I,IH), (AHBMAG(I,J,IH),J=1,NBAND)
c          pause

          THB(I,1,IH) = AHBMAG(I,7,IH) ! F814W
          THB(I,2,IH) = AHBMAG(I,4,IH)-THB(I,1,IH) ! (F606W-F814W)

         END DO 
          CLOSE (3)
        END DO
      END IF
      TMAX=(10.D0**AGE(NSHORT,NLOW+1))*1.D-06
      TMIN=(10.D0**AGE(NLONG,NMAS))*1.D-06
      WRITE(*,107)TMIN,TMAX
      RETURN
      END
C===================================================================            
C===================================================================            
C===================================================================            
      subroutine salpet (emmi,emma,alfa,solve)
      real*8 emmi,emma,alfa,solve,beta,dm
      common /random/ix
      DATA Is/1/
      IF(is.EQ.1) THEN
        ix=1
        is=0
      END IF
      beta=1.0d0-alfa
      dm=emma**beta-emmi**beta
c      write(*,*) emma,emmi,beta,alfa
      pu=ran2(ix)
      solve=(dm*pu+emmi**beta)**(1.0/beta)
c      write(*,*) pu, solve
c      pause
      return
      end
C===================================================================            
C===================================================================            
C===================================================================            
      subroutine gauss (xm,sig,solve)
      real*8 xm,sig,solve
      common /random/ix
      DIMENSION F(85),R(85)
      DATA ig/1/
      IF(ig.EQ.1) THEN
        open(2,file='gauss.dat',status='old')
        DO J=1,85
          READ(2,*)R(J),F(J)
        END DO
        CLOSE (2)
        ig=0
      END IF
      pu=ran2(ix)
      DO I=2,85
         IF(F(I). GE. PU) GO TO 1
      END DO
      I=85
   1  X1=F(I-1)
      X2=F(I)
      Y1=R(I-1)
      Y2=R(I)
      SOLVE=Y1+(Y2-Y1)/(X2-X1)*(PU-X1)
      solve=solve*sig+xm
      return
      end
C#################################################################
C####### Numerical Recipes pag. 272 ###### RANDOM 2 ##############
C################################################################# 
        FUNCTION ran2(idum)
        INTEGER idum,IM1,IM2,IMM1,IA1,IA2,IQ1,IQ2,IR1,IR2,NTAB,NDIV
        REAL ran2,AM,EPS,RNMX
        PARAMETER (IM1=2147483563,IM2=2147483399,AM=1.0/IM1,
     #IMM1=IM1-1,IA1=40014,IA2=40692,IQ1=53668,IQ2=52774,IR1=12211,
     #IR2=3791,NTAB=32,NDIV=1+IMM1/NTAB,EPS=1.2E-7,RNMX=1.-EPS)
        INTEGER idum2,j,k,iv(NTAB),iy
        SAVE iv,iy,idum2
        DATA idum2/123456789/, iv/NTAB*0/, iy/0/
C***************************************************************        
        DATA IRO/0/
C***************** SEME ESTRAZIONE RANDOM **********************
C***************************************************************
      IF(IRO.EQ.0) THEN
       write(*,*)
       WRITE(*,*) 'SEED FOR RANDOM EXTRACTION (NEGATIVE INTEGER) -->'
       READ(*,*) IDUM
       WRITE(*,*) 'SEED---> ',IDUM
       IRO=1
c      pause
      ENDIF
C***************************************************************

        if (idum.le.0) then
          idum=max(-idum,1)
          idum2=idum
          do j=NTAB+8,1,-1
             k=idum/IQ1
             idum=IA1*(idum-k*IQ1)-k*IR1
             if (idum.lt.0) idum=idum+IM1
             if (j.le.NTAB) iv(j)=idum
          enddo
          iy=iv(1)
        endif
        k=idum/IQ1
        idum=IA1*(idum-k*IQ1)-k*IR1
        if (idum.lt.0) idum=idum+IM1
        k=idum2/IQ2
        idum2=IA2*(idum2-k*IQ2)-k*IR2
        if (idum2.lt.0) idum2=idum2+IM2
        j=1+iy/NDIV
        iy=iv(j)-idum2
        iv(j)=idum
        if(iy.lt.1) iy=iy+IMM1
        ran2=min(AM*iy,RNMX)
        return
        END

C###############################################################
C###############################################################
C###############################################################
C===================================================================            
C===================================================================            
C===================================================================            
      SUBROUTINE SINT2(TMIN,TMAX,VT,MT,INDI,BIN,VL)
      IMPLICIT REAL*8(A-H,O-Z)
      PARAMETER (IPUN=500000,IMS=20,IHB=40)
      COMMON/COM1/DATA(IPUN,2),ETA(IPUN)
      COMMON/COM2/TRK(IPUN,2,IMS),VMAS(IMS),AGE(IPUN,IMS)
      COMMON/COM3/THB(IPUN,2,IHB),CORE(IHB),AHB(IPUN,IHB)
      COMMON/COM4/HISOC(IPUN,7)
      COMMON/COM6/NMAS,NLOW,NSHORT,NLONG,NINTE,NHB,NPHB
      COMMON/PRIV/YY1,YY2,YY3,YY4,XX1,XX2,XX3,XX4
      DIMENSION ECO(3),ELU(3),SIGL(2,15),SIGC(2,15),emres(IMS,6)
C ****** LETTURA INIZIALIZZAZIONE SINTETICO *******
      OPEN(5,FILE='inisint.dat',STATUS='OLD')
      READ(5,20)MT,VT
      READ(5,21)PB,PCROWD,ALFA,VL,SP,AVMHB,DELM,BIN
c      WRITE(*,21)PB,PCROWD,ALFA,VL,SP,AVMHB,DELM,BIN
c      PAUSE
      READ(5,23)NERR,DIST
      DO I=1,NERR 
         READ(5,22)SIGL(1,I),SIGL(2,I),SIGC(2,I)
         SIGL(1,I)=SIGL(1,I)-DIST
         SIGC(1,I)=SIGL(1,I)
      END DO
      CLOSE(5)
C     WRITE(*,*)'COLOR SIGMA ERROR: '
C     WRITE(*,22) SIGC
  20  FORMAT(I6,F6.0)
  21  FORMAT(2F6.1,F5.2,F6.2,F7.4,3F6.3)
  22  FORMAT(3F7.3)
  23  FORMAT(I4,F7.3)
  25  FORMAT(2X,'SYNTESYS FOR AGE (10E6) = ',F6.0)
C *************************************************
      IF(VT.GT.TMAX.OR.VT.LT.TMIN) THEN
         WRITE(*,*)'ERROR: AGE OUTSIDE THE ALLOWED RANGE'
         STOP
      ELSE
         WRITE(*,25)VT
      END IF
      IF(ALFA.EQ.1.D0)ALFA=1.001D0
      VT=DLOG10(VT)+6.D0
      MB=MT*PB/100.D0              ! NUM. DI BINARIE
      MS=MT-MB                     ! NUM. DI SINGOLE
      MSC=MS*PCROWD/100.D0         ! NUM. DI SINGOLE CROWDED
      MBC=MB*PCROWD/100.D0         ! NUM. DI BINARIE CROWDED
cc     write(*,*) mb,mt,msc,mbc
cc     pause
C ********* CALCOLO LUMINOSITA' A X=0  *************
      do jj=4,nmas
         if(vt.gt.age(nshort,jj)) go to 88
      end do
      write(*,*)'TURN OFF MASS OUT OF ALLOWED RANGE'
      stop
 88   xx1=age(nshort,jj-3)
      xx2=age(nshort,jj-2)
      xx3=age(nshort,jj-1)
      xx4=age(nshort,jj)
      yy1=trk(nshort,1,jj-3)
      yy2=trk(nshort,1,jj-2)
      yy3=trk(nshort,1,jj-1)
      yy4=trk(nshort,1,jj)
      call cub (A,B,C,D)
      vlto=A*vt**3+B*vt**2+C*vt+D           ! luminosita' a X=0  per T=VT
      write(*,333)vl,vlto
 333  format(1X,'LOWER LUMINOSITY & TO LUMINOSITY: ',2f11.6)
C ******** CALCOLO MASSA LIMITE INFERIORE *********
      jcont=0
      if(vl.ge.vlto) then
        do j=1,nmas
          do k=2,nshort
              if(age(k,j).gt.vt) go to 40
          end do
          WRITE(*,*)'WARNING: LOWER MASS MAY BE AMBIGUOS 1:',vmas(j),age(nshort,j),vt
          go to 444
   40     jcont=jcont+1
          x1=age(k-1,j)
          x2=age(k,j)
          y1=trk(k-1,1,j)
          y2=trk(k,1,j)
          XX3=XX4
          XX4=Y1+(VT-X1)*(Y2-Y1)/(X2-X1) ! lum MASSA JESIMA A t=VT
c          WRITE(*,*)J,XX4,VMAS(j)
          IF(XX4.LE.VL.AND.JCONT.GE.2) GO TO 41
 444      CONTINUE
        END DO
        WRITE(*,*)'SOMETHING WRONG IN SINT 1'
        STOP
   41   YY3=VMAS(J-1)
        YY4=VMAS(J)
        EMMI1=yy3+(yy4-yy3)/(xx4-xx3)*(vl-xx3)     ! prima massa di prova
      else
        DO J=nlow+1,NMAS
          do k=2,nlong
             if(TRK(k,1,j). LT. VL) go to 8
          end do
          WRITE(*,*)'WARNING: LOWER MASS MAY BE AMBIGUOS 2:',vmas(J)
          go to 888
   8      jcont=jcont+1
          x1=TRK(k-1,1,j)
          x2=TRK(k,1,j)
          y1=AGE(k-1,j)
          y2=AGE(k,j)
          XX3=XX4
          XX4=Y1+(VL-X1)*(Y2-Y1)/(X2-X1) ! ETA' MASSA JESIMA A L=VL
C          WRITE(*,*)J,XX4,Vmas(j)
          IF(XX4.LE.VT.AND.JCONT.GE.2) GO TO 9
 888      CONTINUE
        END DO
        WRITE(*,*)'SOMETHING WRONG IN SINT 1'
        STOP
   9    YY3=VMAS(J-1)
        YY4=VMAS(J)
        EMMI1=yy3+(yy4-yy3)/(xx4-xx3)*(vt-xx3)     ! prima massa di prova
      endif
      CALL HR2(VT,EMMI1,100.D0,100.D0,MINI,MAXI)
      DO L=MINI,MAXI
        IF(VT.LE.ETA(L)) GO TO 6
      END DO
   6  X1=ETA(L-1)
      X2=ETA(L)
      Y1=DATA(L-1,1)
      Y2=DATA(L,1)
      ELLU1=Y1+(vt-X1)*(Y2-Y1)/(X2-X1) !luminosita' pr. m di prova a T=VT
      FAT=0.01d0
 77   IF(ELLU1.GT.VL) THEN
         EMMI2=EMMI1*(1.D0+fat)   ! seconda massa di prova
      ELSE
         EMMI2=EMMI1*(1.d0-fat)
      END IF
      CALL HR2(VT,EMMI2,100.D0,100.D0,MINI,MAXI)
      DO L=MINI,MAXI
        IF(VT.LE.ETA(L)) GO TO 7
      END DO
      fat=fat/2.d0
      write(*,*)'FAT REDUCED: ',FAT
      go to 77
  7   X1=ETA(L-1)
      X2=ETA(L)
      Y1=DATA(L-1,1)
      Y2=DATA(L,1)
      ELLU2=Y1+(vt-X1)*(Y2-Y1)/(X2-X1) !luminosita' se. m di prova a T=VT
      EMMI=EMMI1+(EMMI2-EMMI1)/(ELLU2-ELLU1)*(VL-ELLU1)
C******  Calcola massa limite inferiore per sistemi binari : vlbin=vl+0.75 ******
      VLBIN=VL+0.75D0
      jcont=0
      IF (PB.NE.0.OR.PCROWD.NE.0) then
        do j=1,nmas
          do k=2,nshort
              if(age(k,j).gt.vt) go to 50
          end do
          WRITE(*,*)'WARNING: LOWER MASS MAY BE AMBIGUOS 3:',vmas(j)
          go to 555
   50     jcont=jcont+1
          x1=age(k-1,j)
          x2=age(k,j)
          y1=trk(k-1,1,j)
          y2=trk(k,1,j)
          XX3=XX4
          XX4=Y1+(VT-X1)*(Y2-Y1)/(X2-X1) ! lum MASSA JESIMA A t=VT
C          WRITE(*,*)J,XX4,VMAS(j)
          IF(XX4.LE.VLBIN.AND.JCONT.GE.2) GO TO 51
 555      CONTINUE
        END DO
        WRITE(*,*)'SOMETHING WRONG IN SINT 1'
        STOP
  51    YY3=VMAS(J-1)
        YY4=VMAS(J)
        EMMI1=yy3+(yy4-yy3)/(xx4-xx3)*(vlbin-xx3)     ! prima massa di provA      
        CALL HR2(VT,EMMI1,100.D0,100.D0,MINI,MAXI)
        DO L=MINI,MAXI
          IF(VT.LE.ETA(L)) GO TO 52
        END DO
  52    X1=ETA(L-1)
        X2=ETA(L)
        Y1=DATA(L-1,1)
        Y2=DATA(L,1)
        ELLU1=Y1+(vt-X1)*(Y2-Y1)/(X2-X1) !luminosita' pr. m di prova a T=VT
        FAT=0.01d0
  53    IF(ELLU1.GT.VLBIN) THEN
          EMMI2=EMMI1*(1.D0+fat)   ! seconda massa di prova
        ELSE
          EMMI2=EMMI1*(1.d0-fat)
        END IF
        CALL HR2(VT,EMMI2,100.D0,100.D0,MINI,MAXI)
        DO L=MINI,MAXI
          IF(VT.LE.ETA(L)) GO TO 54
        END DO
        fat=fat/2.d0
        write(*,*)'FAT REDUCED: ',FAT
        go to 53
  54    X1=ETA(L-1)
        X2=ETA(L)
        Y1=DATA(L-1,1)
        Y2=DATA(L,1)
        ELLU2=Y1+(vt-X1)*(Y2-Y1)/(X2-X1) !luminosita' se. m di prova a T=VT
        EMMIB=EMMI1+(EMMI2-EMMI1)/(ELLU2-ELLU1)*(VLBIN-ELLU1)
      else
        emmib=emmi
      end if
C ********* CALCOLO MASSA AL FLASH ***************
      DO J=NLOW+4,NMAS
        IF(AGE(NLONG,J). LE. VT) GO TO 11
      END DO
      WRITE(*,*)'SOMETHING WRONG IN SINT 3'
      STOP
  11  XX1=AGE(NLONG,J-3)
      XX2=AGE(NLONG,J-2)
      XX3=AGE(NLONG,J-1)
      XX4=AGE(NLONG,J)
      YY1=VMAS(J-3)
      YY2=VMAS(J-2)
      YY3=VMAS(J-1)
      YY4=VMAS(J)
      CALL CUB(A,B,C,D)
      EMFL=A*VT**3+B*VT**2+C*VT+D     ! MASSA AL FLASH PER T=VT
C ******* CALCOLO MASSA CENTRALE IN HB ******
101   FORMAT(3X,'AVERAGE HB MASS (Mo) --->', F6.3)
      IF(INDI.EQ.2) THEN
        cmassa=AVMHB     ! MASSA MEDIA in HB
        WRITE(*,101) CMASSA
        do jj=4,nhb
           write(*,*) jj, core(jj), cmassa
           if(core(jj).ge.cmassa) go to 33
        end do
        write(*,*)'HB AVERAGE MASS TOO LARGE'
        stop
 33     xx1=core(jj-3)
        xx2=core(jj-2)
        xx3=core(jj-1)
        xx4=core(jj)
        yy1=ahb(nphb,jj-3)
        yy2=ahb(nphb,jj-2)
        yy3=ahb(nphb,jj-1)
        yy4=ahb(nphb,jj)
        call cub (A,B,C,D)
        chba=A*cmassa**3+B*cmassa**2+C*cmassa+D     ! eta' di cmassa a Y=0
        ATHBAM=(10.D0**CHBA)/1.0D6
        write(*,666)ATHBAM,cmassa
  666   format(1x,'HB LIFETIME (Myr) & CENTRAL MASS --->',2F8.3)
C ******** CALCOLO MASSA LIMITE SUPERIORE *********
        DO J=NLOW+4,NMAS
            XX4=DLOG10(10.D0**AGE(NLONG,J)+10.D0**chba)   ! TFLASH+THB
            IF(XX4. LE. VT) GO TO 10
        END DO
        WRITE(*,*)'SOMETHING WRONG IN SINT 2'
        STOP
   10   XX1=DLOG10(10.D0**AGE(NLONG,J-3)+10.D0**chba)
        XX2=DLOG10(10.D0**AGE(NLONG,J-2)+10.D0**chba)
        XX3=DLOG10(10.D0**AGE(NLONG,J-1)+10.D0**chba)
        YY1=VMAS(J-3)
        YY2=VMAS(J-2)
        YY3=VMAS(J-1)
        YY4=VMAS(J)
        CALL CUB(A,B,C,D)
        EMMA=A*VT**3+B*VT**2+C*VT+D     ! MASSA A Y=0 PER T=VT
      END IF
c**************************************************************
      IF(INDI.EQ.3) EMMA=EMFL
      WRITE(*,111)EMMIB,EMMI,EMFL,EMMA
      MSNC=MS-MSC
      MBNC=MB-MBC
      MK=MS+MBNC
      write(*,222)MSNC,MS,Mk,MT
  111 FORMAT(1X,'MASS LIM. (infb, infs, flash, Y=0):',4F10.5)
  222 FORMAT(1X,'NUM. LIM. (SING. SING+CROW, BIN., BIN.+CROW):',4I7)
C ************** LOOP PRINCIPALE SULLE STELLE **************
 553   format('single star', I7)
 663   format('single crowded star')
 773   format('binary star')
 883   format('binary crowded star')
      do j=1,MT
 34      CONTINUE
         IF (J.LE.MSNC) THEN
              WRITE(*,553) J
c              PAUSE
              call salpet(emmi,emma,alfa,emme)
              ISING=1
              DM2=0.d0
              DM3=0.d0
         ELSE if (j.LE.MS) then
              WRITE(*,663)
c              PAUSE
              call salpet(emmib,emma,alfa,emme)
              ising=2
              CALL SALPET(EMMIB,EMMA,ALFA,EM2)
              DM2=EMME-EM2
              DM3=0.d0
         ELSE IF (J.LE.MK) THEN
              WRITE(*,773)
c              PAUSE

              CALL SALPET(EMMIB,EMMA,ALFA,EMME)
              ISING=2
              xm=1.d0
              call gauss (xm,sp,Q)
              DM2=EMME*(1.D0-Q)
              write(*,*) dm2,emme,q
              IF ((EMME*Q).LT.EMMIB) DM2=EMME-EMMIB
              DM3=0.d0
         ELSE
              WRITE(*,883)
c              PAUSE

              CALL SALPET(EMMIB,EMMA,ALFA,EMME)
              ISING=3
              xm=1.d0
              call gauss (xm,sp,Q)
              DM2=EMME*(1.D0-Q)
              IF ((EMME*Q).LT.EMMIB) DM2=EMME-EMMIB
              CALL SALPET(EMMIB,EMMA,ALFA,EM2)
              DM3=EMME-EM2 
         END IF
         do i=1,ising
            IF (I.LT.3) THEN
                EC=EMME+(1-I)*DM2
            ELSE
                EC=EMME-DM3
            END IF
c           if(ec. le. emmi) ec=emmi
            IF(EC. GE. EMMA) go to 34
            IF(EC.GT.EMFL.AND.INDI.EQ.2) THEN
               XM=0.D0
   18          CALL GAUSS (XM,DELM,DMC)
               ECC=CMASSA+DMC
               IF(ECC.GT.CORE(NHB).OR.ECC.LT.CORE(1)) THEN
                 WRITE(*,*)'HB MASS SPREAD TOO LARGE !!!',ECC,CMASSA,EC
                 GO TO 18
               END IF
            ELSE
               ECC=0.D0
            END IF
            CALL HR2(VT,EC,EMFL,ECC,MIN,MAX)
            DO L=MIN,MAX
              IF(VT.LE.ETA(L)) GO TO 12
            END DO
c            L=MAX
c            WRITE(*,*)'AGE OVER LIMITS: ',ec,eta(max),VT
            go to 34
   12       X1=ETA(L-1)
            X2=ETA(L)
            Y1=DATA(L-1,1)
            Y2=DATA(L,1)
            elu(i)=Y1+(vt-X1)*(Y2-Y1)/(X2-X1)
            y1=data(l-1,2)
            y2=data(l,2)
            eco(i)=Y1+(vt-X1)*(Y2-Y1)/(X2-X1)
         end do
         if(ising.eq.1) then
           clu=elu(1)
           cco=eco(1)
           q=0.d0
         ELSE
           V1=ELU(1)              ! FLUSSO M1 V
           B1=ECO(1)+ELU(1)       ! FLUSSO M1 B
           V2=ELU(2)              ! FLUSSO M2 V
           B2=ECO(2)+ELU(2)       ! FLUSSO M2 B
c***********************************************************************************
c*  V=-2.5logF+cost  -->  F=10^((cost-V)/2.5) 
c*  V(binary) = -2.5log(F1+F2)+cost = -2.5log(F1*(1+F2/F1))+cost= V1-2.5log(1+F2/F1) 
c***********************************************************************************
           CLU=V1-2.5*DLOG10(1.D0+10.D0**((V1-V2)*0.4D0)) ! MAGN M1+M2 V
           BLU=B1-2.5*DLOG10(1.D0+10.D0**((B1-B2)*0.4D0)) ! MAGN M1+M2 B
           CCO=BLU-CLU            ! COLORE M1+M2
           IF (ISING.EQ.3) THEN
                V1=CLU
                B1=BLU
                V2=ELU(3)
                B2=ECO(3)+ELU(3)
                CLU=V1-2.5*DLOG10(1.D0+10.D0**((V1-V2)*0.4D0)) ! MAGN M1+M2+M3 V
                BLU=B1-2.5*DLOG10(1.D0+10.D0**((B1-B2)*0.4D0)) ! MAGN M1+M2+M3 B
                CCO=BLU-CLU            ! COLORE M1+M2+M3
           END IF
C          WRITE(9,1111)EMME,V1,B1,EC,V2,B2,CLU,BLU
         END IF
 1111    FORMAT(8F8.3)
         if(SIGL(2,1).GT.0.D0) then         ! Calcolo errore nella magn.
            DO IL=2,NERR
              IF(CLU.LT.SIGL(1,IL))GO TO 15
            END DO
            IL=NERR
   15       X1=SIGL(1,IL-1)
            X2=SIGL(1,IL)
            Y1=SIGL(2,IL-1)
            Y2=SIGL(2,IL)
            SIGMA=Y1+(Y2-Y1)/(X2-X1)*(CLU-X1)
            xm=0.d0
            IF(SIGMA.LT.0.D0)SIGMA=0.0001D0
            call gauss(xm,SIGMA,var)
            clu=clu+var
            HISOC(J,6)=VAR
         end if
         if(SIGC(2,1).GT.0.D0) then         ! Calcolo errore nel B-V
            DO IL=2,NERR
              IF(CLU.LT.SIGC(1,IL))GO TO 16
            END DO
            IL=NERR
   16       X1=SIGC(1,IL-1)
            X2=SIGC(1,IL)
            Y1=SIGC(2,IL-1)
            Y2=SIGC(2,IL)
            SIGMA=Y1+(Y2-Y1)/(X2-X1)*(CLU-X1)
            IF(SIGMA.LT.0.D0)SIGMA=0.0001D0
            xm=0.d0
            call gauss(XM,SIGMA,VAR)
            cco=cco+var
            HISOC(J,7)=VAR
         end if
         HISOC(J,1)=clu
         HISOC(J,2)=cco
         HISOC(J,3)=emme
         HISOC(J,4)=DM2
         HISOC(j,5)=ecc
   1     CONTINUE
      end do
      return
      end
      SUBROUTINE WRITE2(VT,NPU,BIN,VL)
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (IPUN=500000)
      COMMON/COM4/HISOC(IPUN,7)
      COMMON/COM6/NMAS,NLOW,NSHORT,NLONG,NINTE,NVAI1,NVA12
      DIMENSION NCOUNT(100)
C=========================================================================== 
 329  FORMAT('#',6X,'BASTI - SYNTHETIC COLOR-MAGNITUDE DIAGRAM 
     #- v. 1.0 ')
 330  FORMAT('#============================================
     #===================')
 331   FORMAT('#',3X,'mag',6X,'col', 5X, 'Mass', 5X, 'var1', 5X,
     # 'var2', 5X, 'Dmag', 5X, 'Dcol')   
C===========================================================================      
       OPEN(2,FILE='cmd_synt.dat')
       OPEN(4,FILE='flum_synt.dat')
C ********* DIAGRAMMA SINTETICO ********************************************
       WRITE(2,329)
       WRITE(2,330)
       WRITE(2,331)
       WRITE(2,330)
C===========================================================================      
      ETALE=10.D0**(VT-6.D0)
c      WRITE(*,*)'!!!!!! RR LYRAE TRUNCKED !!!!!'
      DO J = 1 , NPU
c        if(hisoc(j,1).ge.1.5d0.or.hisoc(j,2).le.0.17d0.
c     #      or.hisoc(j,2).ge.0.41d0) then                ! RR-LYRAE GAP
            WRITE(2,102)(HISOC(J,L),L=1,7)
c        end if
      END DO
      CLOSE(2)
C ********* FUNZIONE DI LUMINOSITA' ****************************************
      NBIN=AINT(10/BIN)
      DO K=1,NBIN
        NCOUNT(K)=0
      END DO
      DO J=1,NPU
        DO K=1,NBIN
           BLIM=VL-BIN*K
           IF(HISOC(J,1).GT.BLIM) GO TO 2
        END DO
2       if(hisoc(j,1).ge.1.5d0.or.hisoc(j,2).le.0.17d0.
     #      or.hisoc(j,2).ge.0.41d0) ncount(k)=ncount(k)+1 !!!RR-LYRAE GAP
        ncount(k)=ncount(k)+1
      END DO
      ncul=0
      DO K=NBIN,1,-1
         CEN=VL-BIN*K+BIN/2.D0
         NCUL=ncount(k)+ncul
         IF(NCUL.GT.0) THEN
           ALOGC=dlog10(dble(ncul))
C           ALOGD=dlog10(dble(ncount(k)))
C           WRITE(4,105)CEN,NCOUNT(K),NCUL
           WRITE(4,105)CEN,NCOUNT(K),ncul,alogc
         END IF
      END DO
      close (4)
      RETURN
  100 FORMAT(I5,F10.0)
  102 FORMAT(7F9.5)
  105 FORMAT(F9.5,2I6,2f9.5)
      END
      SUBROUTINE TRAK
      PARAMETER (IPUN=500000)
      IMPLICIT REAL*8(A-H,O-Z)
      COMMON/COM1/DATA(IPUN,2),ETA(IPUN)
      WRITE(*,*)'GIVI ME MASS'
      READ(*,*) VM
      EMMAX=100.D0
      CALL HR2(0.D0,VM,EMMAX,0.D0,MIN,MAX)
      OPEN(7,FILE='TRACK')
      DO J=MIN,MAX
         WRITE(7,100)ETA(J),DATA(J,1),DATA(J,2)
      END DO
      CLOSE(7)
      STOP
  100 FORMAT(F13.8,2F9.4)
      END
      SUBROUTINE HR2(VT,VM,VMFL,VMC,MIN,MAX)
      IMPLICIT REAL*8 (A-H,O-Z)
      PARAMETER (IPUN=500000,IMS=20,INN=40)
      COMMON/COM1/DATA(IPUN,2),ETA(IPUN)
      COMMON/COM2/TRK(IPUN,2,IMS),VMAS(IMS),AGE(IPUN,IMS)
      COMMON/COM3/THB(IPUN,2,INN),CORE(INN),AHB(IPUN,INN)
      COMMON/COM6/NMAS,NLOW,NSHORT,NLONG,NINTE,NHB,NPHB
      COMMON/PRIV/YY1,YY2,YY3,YY4,XX1,XX2,XX3,XX4
c     IF(VM.LT.VMAS(1).OR.VM.GT.VMAS(NMAS)) THEN
      IF(VM.GT.VMAS(NMAS)) THEN
          WRITE(*,*)'MASS OUTSIDE THE ALLOWED RANGE',VM
          STOP
      ENDIF
      IF(VM. LE. VMAS(NLOW+1)) THEN
         MAX=NSHORT
         MIN=1
         IHB=0
         ini=4
      ELSE IF(VM.GT.VMFL) THEN
         MAX=NPHB
         MIN=1
         IHB=1
         ini=nlow+4
      ELSE
         MIN=1
         MAX=NLONG
         IHB=0
         ini=nlow+4
      ENDIF
c        if(ihb.eq.1) then
c        write(*,*) ihb
c        endif
C ********* RICERCA INTERVALLO IN MASSA O IN MASSA DI CORE *******
      DO K = ini , nmas
         IF(VMAS(K). GE. VM) GO TO 2
      END DO
      WRITE(*,*)'SOMETHING WRONG IN HR 1'
      STOP
   2  xx1=VMAS(k-3)
      xx2=VMAS(k-2)
      xx3=VMAS(k-1)
      xx4=VMAS(k)
      if(ihb.eq.1)then
         yy1=age(nlong,k-3)
         yy2=age(nlong,k-2)
         yy3=age(nlong,k-1)
         yy4=age(nlong,k)
         call cub (A,B,C,D)
         etafl=A*vm**3+B*vm**2+C*vm+D
      end if
      IF(IHB.EQ.1) THEN
         DO KHB = 4 , NHB
           IF(CORE(KHB). GE. VMC) GO TO 3
         END DO
         WRITE(*,*)'SOMETHING WRONG IN HR 2',VMC
         STOP
    3    CONTINUE
         IF(KHB.GT.4.AND.KHB.NE.NHB) KHB=KHB+1
         xx1=CORE(KHB-3)
         xx2=CORE(KHB-2)
         xx3=CORE(KHB-1)
         xx4=CORE(KHB)
c         write(*,*) xx1,xx2,xx3,xx4,vmc
c         pause
      END IF
      DO L=MIN,MAX
         IF(IHB.EQ.0) THEN
            yy1=trk(L,1,k-3)
            yy2=trk(L,1,k-2)
            yy3=trk(L,1,k-1)
            yy4=trk(L,1,k)
            call cub (A,B,C,D)
            DATA(L,1)=A*VM**3+B*VM**2+C*VM+D
            yy1=trk(L,2,k-3)
            yy2=trk(L,2,k-2)
            yy3=trk(L,2,k-1)
            yy4=trk(L,2,k)
            call cub (A,B,C,D)
            DATA(L,2)=A*VM**3+B*VM**2+C*VM+D
            yy1=age(L,k-3)
            yy2=age(L,k-2)
            yy3=age(L,k-1)
            yy4=age(L,k)
            call cub (A,B,C,D)
            eta(L)=A*VM**3+B*VM**2+C*VM+D
         ELSE
            yy1=thb(L,1,khb-3)
            yy2=thb(L,1,khb-2)
            yy3=thb(L,1,khb-1)
            yy4=thb(L,1,khb)
            call cub (A,B,C,D)
            DATA(L,1)=A*VMC**3+B*VMC**2+C*VMC+D
            yy1=thb(L,2,khb-3)
            yy2=thb(L,2,khb-2)
            yy3=thb(L,2,khb-1)
            yy4=thb(L,2,khb)
            call cub (A,B,C,D)
            DATA(L,2)=A*VMC**3+B*VMC**2+C*VMC+D
            yy1=ahb(L,khb-3)
            yy2=ahb(L,khb-2)
            yy3=ahb(L,khb-1)
            yy4=ahb(L,khb)
            call cub (A,B,C,D)
            eta(L)=A*VMC**3+B*VMC**2+C*VMC+D
            ETA(L)=DLOG10(10.D0**ETA(L)+10.D0**ETAFL)
         END IF
      END DO
      RETURN
      END
