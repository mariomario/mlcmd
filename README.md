# Cluster Ages and More from machinE Learning (CAMEL)

## Files
1. montecarlizza.R R script that prepares train and test set
2. learn.R script that trains ML models
3. nonInteracriveSanti.exp expect script to use the synthetic CMD generator non-interactively
4. sint_cmd.e synthetic CMD generator binary
5. header.template template for the header of the inisint.dat file
6. subs.sh runs keyword substitutions on the template

## Usage
montecarlizza.R contains function montecarlizza_runna_rileggi(...) that produces a synthetic CMD for a given combination of parameters and random seed
and function generate_training_set() that turns the synthetic CMD into a feature table paired with labels (the parameters used to generate the CMD)
learn.R trains the model on the pre-made train set and tests on test set.
